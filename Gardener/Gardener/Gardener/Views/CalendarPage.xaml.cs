﻿using Gardener.ViewModels;
using Syncfusion.SfCalendar.XForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gardener.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CalendarPage : ContentPage
    {
        CalendarPageViewModel vm;
        public CalendarPage()
        {
            
            InitializeComponent();

            vm = new CalendarPageViewModel();
            this.BindingContext = vm;

            MonthViewSettings sett = new MonthViewSettings();

        }
        
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await ((CalendarPageViewModel)this.BindingContext).LoadAllData();
        }

    }
}
