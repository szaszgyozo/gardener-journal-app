﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gardener.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gardener.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewReminderPage : ContentPage
    {
        NewReminderPageViewModel vm;
        public NewReminderPage()
        {
            vm = new NewReminderPageViewModel();
            this.BindingContext = vm;

            InitializeComponent();
        }

        private void DatePicker_DateSelected(object sender, DateChangedEventArgs e)
        {
            vm.ReminderDate = e.NewDate;
            //MyDatepicker.Date = e.NewDate;

        }
    }
}