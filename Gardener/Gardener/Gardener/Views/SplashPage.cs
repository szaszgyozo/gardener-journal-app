﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Gardener
{
    public class SplashPage : ContentPage
    {
        Image welcomeImage;

        public SplashPage()
        {
            var sub = new AbsoluteLayout();
            welcomeImage = new Image
            {
                Source = "welcomeLogo.png",
                WidthRequest = 100,
                HeightRequest = 100
            };
            AbsoluteLayout.SetLayoutFlags(welcomeImage,
                AbsoluteLayoutFlags.PositionProportional);
            AbsoluteLayout.SetLayoutBounds(welcomeImage,
                new Rectangle(0.5, 0.5, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));
            
            

            sub.Children.Add(welcomeImage);

            this.BackgroundImageSource = "welcomeBackground.jpg";
            //this.BackgroundColor = Color.FromHex("#FFFFFF");
            this.Content = sub;
            
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await welcomeImage.ScaleTo(1, 2000);                                //Time-consuming processes such as initialization
            await welcomeImage.ScaleTo(0.9, 1500, Easing.Linear);
            await welcomeImage.ScaleTo(150, 1200, Easing.Linear);
            Application.Current.MainPage = new TabbedNavigationPage();    //After loading  MainPage it gets Navigated to our new Page
        }

    }
}
