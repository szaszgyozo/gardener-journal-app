﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gardener.Models
{
    public class ReminderEntry
    {
        #region Fields
        private Guid _id;
        private string _title;
        private DateTime _entryDate;
        private DateTime _reminderDate;
        #endregion


        #region Constructors
        public ReminderEntry()
        {

        }
        #endregion


        #region Properties
        public Guid Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }


        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }
        }

        public DateTime EntryDate
        {
            get
            {
                return _entryDate;
            }
            set
            {
                _entryDate = value;
            }
        }

        public DateTime ReminderDate
        {
            get
            {
                return _reminderDate;
            }
            set
            {
                _reminderDate = value;
            }
        }
        #endregion

        #region Public Methods

        #endregion



    }
}
