﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gardener
{
    public class JournalEntry
    {
        #region Fields

        private Guid _id;
        private string _title;
        private string _description;
        private DateTime _entryDate;
        private string[] _images64;

        #endregion

        #region Constructors

        public JournalEntry()
        {

        }

        #endregion

        #region Properties
        public Guid Id 
        { 
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public string Title 
        { 
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            } 
        }

        public string  Description 
        { 
            get
            {
                return _description;
            } 
            set
            {
                _description = value;
            }
        }

        public DateTime EntryDate 
        {
            get
            {
                return _entryDate;
            } 
            set
            {
                _entryDate = value;
            } 
        }

        public string[] Images64
        {
            get
            {
                return _images64;
            }
            set
            {
                _images64 = value;
            }
        }

        #endregion 
    }
}
