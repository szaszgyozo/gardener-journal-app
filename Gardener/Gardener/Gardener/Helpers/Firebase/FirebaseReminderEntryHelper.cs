﻿using System;
using Firebase.Database;
using Firebase.Database.Query;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Gardener.Models;
using System.Linq;

namespace Gardener.Helpers
{
    public class FirebaseReminderEntryHelper
    {
        readonly FirebaseClient firebaseClient = new FirebaseClient("https://gardener-journal.firebaseio.com/");
        private readonly string reminderEntryObjectName = "Reminder Entries";

        public async Task AddReminderEntry(ReminderEntry reminderEntry)
        {
            await firebaseClient.Child(reminderEntryObjectName).PostAsync(reminderEntry);

        }

        public async Task<ObservableCollection<ReminderEntry>> GetAllReminderEntries()
        {
            var result = (await firebaseClient
                .Child(reminderEntryObjectName)
                .OnceAsync<ReminderEntry>())
                .Select(entry => new ReminderEntry
                {
                    Id = entry.Object.Id,                   
                    Title = entry.Object.Title,
                    EntryDate = entry.Object.EntryDate,
                    ReminderDate = entry.Object.ReminderDate,
                });
            return new ObservableCollection<ReminderEntry>(result);
        }

        public async Task UpdateReminderEntry(ReminderEntry reminderEntry)
        {
            var updateEntry = (await firebaseClient
                .Child(reminderEntryObjectName)
                .OnceAsync<ReminderEntry>())
                .FirstOrDefault(e => e.Object.Id == reminderEntry.Id);

            await firebaseClient
                .Child(reminderEntryObjectName)
                .Child(updateEntry.Key)
                .PutAsync(reminderEntry);
        }

        public async Task DeleteReminderEntry(Guid entryId)
        {
            var deleteEntry = (await firebaseClient
                .Child(reminderEntryObjectName)
                .OnceAsync<ReminderEntry>())
                .FirstOrDefault(e => e.Object.Id == entryId);
            await firebaseClient.Child(reminderEntryObjectName).Child(deleteEntry.Key).DeleteAsync();
        }
    }
}
