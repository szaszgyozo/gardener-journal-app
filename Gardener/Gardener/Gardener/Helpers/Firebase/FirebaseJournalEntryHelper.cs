﻿using Firebase.Database;
using Firebase.Database.Query;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Gardener.Helpers
{
    public class FirebaseJournalEntryHelper
    {
        readonly FirebaseClient firebaseClient = new FirebaseClient("https://gardener-journal.firebaseio.com/");
        private readonly string journalEntryObjectName = "Journal Entries";

        public async Task AddJournalEntry(JournalEntry journalEntry)
        {
            await firebaseClient.Child(journalEntryObjectName).PostAsync(journalEntry);
            
        }

        public async Task<ObservableCollection<JournalEntry>> GetAllJournalEntries()
        {
            var result = (await firebaseClient
                .Child(journalEntryObjectName)
                .OnceAsync<JournalEntry>())
                .Select(entry => new JournalEntry
                {
                    Id = entry.Object.Id,
                    EntryDate = entry.Object.EntryDate,
                    Title = entry.Object.Title,
                    Description = entry.Object.Description,
                    Images64 = entry.Object.Images64
                });
            return new ObservableCollection<JournalEntry>(result);
        }

        public async Task UpdateJournalEntry(JournalEntry journalEntry)
        {
            var updateEntry = (await firebaseClient
                .Child(journalEntryObjectName)
                .OnceAsync<JournalEntry>())
                .FirstOrDefault(e => e.Object.Id == journalEntry.Id);

            await firebaseClient
                .Child(journalEntryObjectName)
                .Child(updateEntry.Key)
                .PutAsync(journalEntry);
        }

        public async Task DeleteJournalEntry(Guid entryId)
        {
            var deleteEntry = (await firebaseClient
                .Child(journalEntryObjectName)
                .OnceAsync<JournalEntry>())
                .FirstOrDefault(e => e.Object.Id == entryId);
            await firebaseClient.Child(journalEntryObjectName).Child(deleteEntry.Key).DeleteAsync();
        }
    }
}

