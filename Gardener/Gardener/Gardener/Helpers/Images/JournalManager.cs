﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gardener.Helpers
{
    public sealed class JournalManager
    {

        #region Fields

        private static JournalManager _instance;
        private static object _locker = new object();

        #endregion

        #region Constructors

        private JournalManager()
        {

        }

        #endregion

        #region Properties

        public static JournalManager Instance
        {
            get
            {
                lock (_locker)
                {
                    if (_instance == null)
                    {
                        _instance = new JournalManager();

                    }
                    return _instance;
                }
            }
        }


        #endregion

        #region Public Methods

        public void loadAllData()
        {

        }

        #endregion

    }
}
