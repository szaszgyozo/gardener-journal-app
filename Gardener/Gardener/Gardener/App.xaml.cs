﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gardener
{
    public partial class App : Application
    {
        public App()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MjUyMzI5QDMxMzgyZTMxMmUzMEJqNWNVTkxTRG8yTU96cHQwU0FhMi9YVDV2ZUduWWlsdmtxaGU0dXlCL0U9");

            InitializeComponent();

            MainPage = new SplashPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
