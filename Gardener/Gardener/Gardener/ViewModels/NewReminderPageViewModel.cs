﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Gardener.Helpers;
using Gardener.Models;
using Xamarin.Forms;

namespace Gardener.ViewModels
{
    public class NewReminderPageViewModel : INotifyPropertyChanged
    {
        private readonly FirebaseReminderEntryHelper firebaseReminderHelper = new FirebaseReminderEntryHelper();
        
        #region Fields

        private DateTime _entryDate;
        private DateTime _reminderDate;
        private string _title;

        #endregion


        #region Constructors
        public NewReminderPageViewModel()
        {
            AddReminderCommand = new Command(async () =>
            {
                var newReminder = new ReminderEntry() { Id = Guid.NewGuid(), EntryDate = EntryDate, ReminderDate = ReminderDate, Title = Title};
                EntryDate = DateTime.Now;
                ReminderDate = DateTime.Now;
                Title = String.Empty;

                await firebaseReminderHelper.AddReminderEntry(newReminder);

            });
        }

        #endregion


        #region properties

        public DateTime EntryDate
        {
            get
            {
                return _entryDate;
            }
            set
            {
                _entryDate = DateTime.Now;
                NotifyPropertyChanged();
            }
        }

        public DateTime ReminderDate
        {
            get
            {
                return _reminderDate;
            }
            set
            {
                _reminderDate = value;
                NotifyPropertyChanged();
            }
        }

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
                NotifyPropertyChanged();
            }
        }


        public Command AddReminderCommand { get; set; }

        #endregion

        #region INotify

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

    }
}
