﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Gardener.Helpers;
using Plugin.Media.Abstractions;
using System.Collections.ObjectModel;
using Plugin.Media;
using System.Runtime.CompilerServices;

namespace Gardener.ViewModels
{
    public class NewPostPageViewModel : INotifyPropertyChanged
    {

        private readonly FirebaseJournalEntryHelper firebaseJournalHelper = new FirebaseJournalEntryHelper();

        private DateTime _postDate;
        private string _postTitle;
        private string _postDescription;
        private ObservableCollection<ImageSource> _imageSources = new ObservableCollection<ImageSource>();
        private List<string> _images64 = new List<string>();


        public NewPostPageViewModel()
        {
            

            AddEntryCommand = new Command(async () =>
            {
                var newEntry = new JournalEntry() { Id = Guid.NewGuid(), EntryDate = PostDate, Title = PostTitle, Description = PostDescription, Images64 = ImagesToSave64.ToArray()};
                PostDate = DateTime.MinValue;
                PostTitle = String.Empty;
                PostDescription = String.Empty;
                LocalImageSources.Clear();
                ImagesToSave64.Clear();
                await firebaseJournalHelper.AddJournalEntry(newEntry);

            });

            AddPhotoCommand = new Command(async () =>
            {
                var images = new ObservableCollection<ImageSource>();

                await CrossMedia.Current.Initialize();

                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await Application.Current.MainPage.DisplayAlert("Not supported", "Your device does not currently support this functionality", "Ok");
                    return;
                }

                var mediaOptions = new PickMediaOptions()
                {
                    PhotoSize = PhotoSize.MaxWidthHeight
                };

                var selectedImageFile = await CrossMedia.Current.PickPhotoAsync(mediaOptions);


                LocalImageSources.Add(ImageSource.FromStream(() => selectedImageFile.GetStream()));

                var imageArray = System.IO.File.ReadAllBytes(selectedImageFile.Path);
                string base64ImageRepresentation = Convert.ToBase64String(imageArray);

                ImagesToSave64.Add(base64ImageRepresentation);



            });

            TakePhotoCommand = new Command(async () =>
            {
                var images = new ObservableCollection<ImageSource>();

                await CrossMedia.Current.Initialize();

                if (!CrossMedia.Current.IsTakePhotoSupported || !CrossMedia.Current.IsCameraAvailable)
                {
                    await Application.Current.MainPage.DisplayAlert("Not supported", "Your device does not currently support this functionality", "Ok");
                    return;
                }

                var mediaOptions = new StoreCameraMediaOptions()
                {
                    PhotoSize = PhotoSize.Medium
                };
                // if you want to take a picture use TakePhotoAsync instead of PickPhotoAsync
                var selectedImageFile = await CrossMedia.Current.TakePhotoAsync(mediaOptions);


                LocalImageSources.Add(ImageSource.FromStream(() => selectedImageFile.GetStream()));
                


            });



        }


        #region properties
        

        public ObservableCollection<ImageSource> LocalImageSources
        {
            get
            {
                return _imageSources;
            }
            set
            {
                _imageSources = value;
                NotifyPropertyChanged();
            }
        }
        
        public string PostTitle
        {
            get
            {
                return _postTitle;
            }
            set
            {
                _postTitle = value;
                NotifyPropertyChanged();
            }
        }

        public DateTime PostDate
        {
            get 
            {
                return _postDate;
            }
            set
            {
                _postDate = DateTime.Now;
                NotifyPropertyChanged();
            }
        }

        public string PostDescription
        {
            get
            {
                return _postDescription;
            }
            set
            {
                _postDescription = value;
                NotifyPropertyChanged();
            }
        }

        public List<string> ImagesToSave64
        {
            get
            {
                return _images64;
            }
            set
            {
                _images64 = value;
            }
        }
        

        public Command AddEntryCommand { get; set; }

        public Command AddPhotoCommand { get; set; }

        public Command TakePhotoCommand { get; set; }

        #region public methods

        #endregion



        #endregion

        #region INotify
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion


    }
}
