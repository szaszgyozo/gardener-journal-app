﻿using System;
using System.Collections.Generic;
using Gardener.Helpers;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Syncfusion.SfCalendar.XForms;
using System.Drawing;
using Gardener.Views;

namespace Gardener.ViewModels
{
    public class CalendarPageViewModel : INotifyPropertyChanged
    {
        private readonly FirebaseJournalEntryHelper firebaseJournalHelper = new FirebaseJournalEntryHelper();
        private readonly FirebaseReminderEntryHelper firebaseReminderHelper = new FirebaseReminderEntryHelper();

        private CalendarEventCollection _calendarInlineEvents = new CalendarEventCollection();
        private CalendarInlineEvent _selectedInlineEvent;
        private bool _selectedDateHasEvents;

        public CalendarPageViewModel()
        {

        }

        #region properties

        

        public bool SelectedDateHasEvents
        {
            get
            {
                return _selectedDateHasEvents;
            }
            set
            {
                _selectedDateHasEvents = value;
                NotifyPropertyChanged();
            }
        }

        public CalendarEventCollection CalendarInlineEvents
        {
            get
            {
                return _calendarInlineEvents;
            }
            set
            {
                _calendarInlineEvents = value;
                NotifyPropertyChanged();
            }
        }

        public CalendarInlineEvent SelectedInlineEvent
        {
            get
            {
                return _selectedInlineEvent;
            }
            set
            {
                _selectedInlineEvent = value;
                NotifyPropertyChanged();
            }
        }


        #endregion

        #region Public Methods
        public async Task LoadAllData()
        {
            CalendarInlineEvents.Clear();
            await LoadAllJournalEntries();
            await LoadAllReminderEntries();
            NotifyPropertyChanged("CalendarInlineEvents");
        }

        public async Task LoadAllJournalEntries()
        {
            var journalEntries = await firebaseJournalHelper.GetAllJournalEntries();

            foreach (var item in journalEntries)
            {
                CalendarInlineEvent newEntry = new CalendarInlineEvent { StartTime = item.EntryDate, EndTime = item.EntryDate, IsAllDay = false, Subject = item.Title, Color = Color.DarkSeaGreen };


                CalendarInlineEvents.Add(newEntry);
            }
        }

        public async Task LoadAllReminderEntries()
        {
            var reminderEntries = await firebaseReminderHelper.GetAllReminderEntries();
            foreach (var item in reminderEntries)
            {
                CalendarInlineEvent newEntry = new CalendarInlineEvent { StartTime = item.ReminderDate, EndTime = item.ReminderDate, IsAllDay = false, Subject = item.Title, Color = Color.LightBlue };


                CalendarInlineEvents.Add(newEntry);
            }
        }

        #endregion

        #region INotify

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}
